package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class User {
	
	private String username;
	private String password;
	
	//DB CONNECTION
	Connection conn = null;
	Statement stmt = null;
	
	//constructor
		public User() {
			
			try {
				
				//Register driver;
				java.sql.DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				
				//Connection
				this.conn = DriverManager.getConnection("jdbc:mysql://localhost/traveltrail?user=root&password=");

				//Create Statement
				this.stmt =  conn.createStatement();

			}catch(Exception ex) {
				System.out.println(ex.getMessage());
			}
			
		}


	public String getUsername() {
			return username;
		}


		public void setUsername(String username) {
			this.username = username;
		}


		public String getPassword() {
			return password;
		}


		public void setPassword(String password) {
			this.password = password;
		}


	public boolean connect(String username, String password) {
		try {
			//RUN SQL
			
			String sql = "SELECT count(*) as count  FROM `user` WHERE username = '" + username + "' AND password = '" + password + "'";
			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);
			rs.next();
			
			int count = rs.getInt("count");
			
			if (count == 1 ) 
				return true; //username and password are corrects!
			else 
				return false; //incorrect username / password!
			

		}catch(Exception ex) {
			System.out.println("error on connect " + ex.getMessage());
			return false;
		}
	}
	
}
